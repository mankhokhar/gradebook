/**
this program only works if your classss has max 100 students
and that the user is giving a .csv file in which continuous data is given
*/

# include <iostream>
# include <fstream>
#include <string>
using namespace std;

/**
dataStructures are being created here
*/
struct student {
	string fname, lname;
	int cmsID, assi1, assi2, assi3, qui1, qui2, qui3, oht1, oht2, ese;
	double agg;
	string grade;

};

struct maxMarks {
	int max_as1, max_as2, max_as3, max_q1, max_q2, max_q3, max_o1, max_o2, max_ese;
};



/**
function to calculate student aggregate 
*/
double calcAgg(student stu,maxMarks mks ){
	 double agg =
	 ((((double)stu.qui1 + (double)stu.qui2 + (double)stu.qui3) / ((double)mks.max_q1 + (double)mks.max_q2 + (double)mks.max_q3)) * 10 )+
	 ((((double)stu.assi1 + (double)stu.assi2 + (double)stu.assi3) / ((double)mks.max_as1 + (double)mks.max_as2 + (double)mks.max_as3)) * 10) +
	 ((((double)stu.oht1 + (double)stu.oht2) / ((double)mks.max_o1 + (double)mks.max_o2)) * 40) +
	 (((double)stu.ese / (double)mks.max_ese) * 40);
	 return agg;
}
/**
function to convent strings to integers used in reading the file
*/
int strToint(string s) {
	if (s.empty()) {
		return 0;
	}
	return stoi(s);
}


/**
function for sorting the students based on their agg it takes an array and sort it
*/
void StudentSort(student stu[])
{
	int i, j, flag = 1;
	student temp;
	int stuLength = 100;
	for (i = 1; (i <= stuLength) && flag; i++)
	{
		flag = 0;
		for (j = 0; j < (stuLength - 1); j++)
		{
			if (stu[j + 1].agg > stu[j].agg)
			{
				temp = stu[j];
				stu[j] = stu[j + 1];
				stu[j + 1] = temp;
				flag = 1;
			}
		}
	}
	return;
}

/**
main method
*/
int main(int argc, char * argv[]) {

	string userfileName;
	string fileName;
	cout << "Enter the file name(just the name of csv file without extension: ";
	cin >> userfileName;
	fileName = userfileName + ".csv";
	/**
	reading the file
	*/
	ifstream file(fileName);

	if (!file.is_open()) {
		cout << "File could not be opened" << endl;
	}

	int max_st;
	cout << "Enter the max number of students :";
	cin >> max_st ;

	student stuData[100];
	maxMarks mxMrks;
	int i = 0;
	string chara;
	
	getline(file, chara);
	/** 
	to get max marks
	*/
	getline(file, chara, ',');
	getline(file, chara, ',');
	getline(file, chara, ',');
	getline(file, chara, ',');

	mxMrks.max_as1 = strToint(chara);
	getline(file, chara, ',');
	mxMrks.max_q1 = strToint(chara);
	getline(file, chara, ',');
	mxMrks.max_q2 = strToint(chara);
	getline(file, chara, ',');
	mxMrks.max_q3 = strToint(chara);
	getline(file, chara, ',');
	mxMrks.max_as2 = strToint(chara);
	getline(file, chara, ',');
	mxMrks.max_as3 = strToint(chara);
	getline(file, chara, ',');
	mxMrks.max_o1 = strToint(chara);
	getline(file, chara, ',');
	mxMrks.max_o2 = strToint(chara);
	getline(file, chara, ',');
	mxMrks.max_ese = strToint(chara);
	
	getline(file, chara);

	while (getline(file, chara, ',')) {
		if (chara.empty()) break;

		stuData[i].cmsID = stoi(chara);

		getline(file, stuData[i].fname, ',');
		getline(file, stuData[i].lname, ',');
		getline(file, chara, ',');
		stuData[i].qui1 = strToint(chara);
		getline(file, chara, ',');
		stuData[i].assi1 = strToint(chara);
		getline(file, chara, ',');
		stuData[i].assi2 = strToint(chara);
		getline(file, chara, ',');
		stuData[i].assi3 = strToint(chara);
		getline(file, chara, ',');
		stuData[i].qui2 = strToint(chara);
		getline(file, chara, ',');
		stuData[i].qui3 = strToint(chara);
		getline(file, chara, ',');
		stuData[i].oht1 = strToint(chara);
		getline(file, chara, ',');
		stuData[i].oht2 = strToint(chara);
		getline(file, chara, ',');
		stuData[i].ese = strToint(chara);
		getline(file, chara, '\n');
		stuData[i].agg = calcAgg(stuData[i], mxMrks);
		i++;
	}
	/** 
	opening files to be written
	*/
	ofstream A_file, B_pos_file, B_file, C_pos_file, C_file, D_pos_file, D_file, F_file;
	A_file.open(userfileName + "-A.csv");
	B_pos_file.open(userfileName + "-Bplus.csv");
	B_file.open(userfileName + "-B.csv");
	C_pos_file.open(userfileName + "-Cplus.csv");
	C_file.open(userfileName + "-C.csv");
	D_pos_file.open(userfileName + "-Dplus.csv");
	D_file.open(userfileName + "-D.csv");
	F_file.open(userfileName + "-F.csv");

	/**
	sorting and file writing
	*/
	StudentSort(stuData);
	cout << "After Sorting" << "\n" << endl;
	cout << endl;
	int j = 0;
	while (j < i) {
		double grade_percent = (double(j+1)/double(i+1) * 100);
		if (grade_percent >= 90) {
			stuData[j].grade = "F";
			F_file << stuData[j].cmsID <<"," << stuData[j].fname <<","<< stuData[j].lname<< "\n";
		}
		else if (grade_percent >= 0 && grade_percent < 10) {
			stuData[j].grade = "A";
			A_file << stuData[j].cmsID << "," << stuData[j].fname << "," << stuData[j].lname << "\n";
		}

		else if (grade_percent >= 80 && grade_percent < 90) {
			stuData[j].grade = "D";
			D_file << stuData[j].cmsID << "," << stuData[j].fname << "," << stuData[j].lname << "\n";
		}

		else if (grade_percent >= 70 && grade_percent < 80) {
			stuData[j].grade = "D+";
			D_pos_file << stuData[j].cmsID << "," << stuData[j].fname << "," << stuData[j].lname << "\n";
		}
		
		else if (grade_percent >= 60 && grade_percent < 70 ){
			stuData[j].grade = "C";
			C_file << stuData[j].cmsID << "," << stuData[j].fname << "," << stuData[j].lname << "\n";
		}

		else if (grade_percent >=50 && grade_percent < 60) {
			stuData[j].grade = "C+";
			C_pos_file << stuData[j].cmsID << "," << stuData[j].fname << "," << stuData[j].lname << "\n";
		}
		else if (grade_percent >= 25 && grade_percent < 50) {
			stuData[j].grade = "B";
			B_file << stuData[j].cmsID << "," << stuData[j].fname << "," << stuData[j].lname << "\n";
		}
		else { 
			stuData[j].grade = "B+";
			B_pos_file << stuData[j].cmsID << "," << stuData[j].fname << "," << stuData[j].lname << "\n";
		}

		j++;
	}
	
	system("pause");
	return 0;
}
